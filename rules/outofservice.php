<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Хостел в центре Одессы">
    <meta name="author" content="http://mama-hostel.com">
    <title>Правила - Mama Hostel in Odessa</title>
    
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../source/jquery.fancybox.css" type="text/css" media="screen" >
    <link rel="stylesheet/less" href="../css/style.less">
	
	<style>
	
		body {
			background-color:  #CCD9F9;
		}
		
		.content {
			width: 75%;
			margin: 0 auto;
			background-color:  #ddD9F9;
			padding: 25px;
		}
		
		h1, h2 {
			color: #1C60CF;
			margin: 0 auto;
			padding: 10px;
		}
		ul {
			list-style-image: url(../img/galochka.png);
		}
	</style> 

 
</head>

<body>
    <header>
        <div class="overlay0">
                <div class="header1">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="logo">
                                    <a href="index.html"><img src="../img/hostellogo.png" width="60" height="60" alt="logo"><span class="lg1">Mama Hostel</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <h2>г. Одесса, ул. Нежинская, 58</h2>
                            </div>
                            <div class="col-sm-3">
                                <p class="tell">+38 068 792 72 10
                                    <br> +38 066 429 75 78
									<br><a href="mailto:mamahostel.od@gmail.com">mamahostel.od@gmail.com</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div>

                    <nav class="navbar navbar-inverse" role="navigation">
                        
						<div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav">
                                <li><a href="../#three">Номера и цены</a>
                                </li>
                                <li><a href="../#whywe">Почему Mama Hostel</a>
                                </li>
                                <li><a href="../#slider">Хостел в деталях</a>
                                </li>
                                <li><a href="../#otz">Отзывы</a>
                                </li>
                                <li><a href="../#howto">Как добраться</a>
                                </li>
                            </ul>
                        </div>
                        <!--.nav-collapse -->

                    </nav>


<div class='content'>
	<h1>Упс...Онлайн бронирование временно не доступно (</h1>
	<p>Сожалеем, чтоб узнать о наличии мест, придется перезвонить по телефону или написать емайл  <strong><a href="mailto:mamahostel.od@gmail.com">mamahostel.od@gmail.com</a></strong> </p>
	<p>Мы уже работаем над решением этой проблемы, спасибо за ваше терпение!</p>
	<h2>Так же можете перезвонить по телефону +38 068 792 72 10</h2>
	<p>Еще один телефон в верхнем правом углу сайта</p>
	
	
</div>
                    
	
		
		<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function (d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter29927109 = new Ya.Metrika({
						id:29927109,
						clickmap:true,
						trackLinks:true,
						accurateTrackBounce:true,
						webvisor:true,
						trackHash:true
					});
				} catch(e) { }
			});

			var n = d.getElementsByTagName("script")[0],
				s = d.createElement("script"),
				f = function () { n.parentNode.insertBefore(s, n); };
			s.type = "text/javascript";
			s.async = true;
			s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else { f(); }
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript><div><img src="//mc.yandex.ru/watch/29927109" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->
													

	
</body>

</html>



