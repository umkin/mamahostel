<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Хостел в центре Одессы">
    <meta name="author" content="http://mama-hostel.com">
    <title>Предоплата - Mama Hostel in Odessa</title>
    
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../source/jquery.fancybox.css" type="text/css" media="screen" >
    <link rel="stylesheet/less" href="../css/style.less">
	
	<style>
	
		body {
			background-color:  #CCD9F9;
		}
		
		.content {
			width: 75%;
			margin: 0 auto;
			background-color:  #ddD9F9;
			padding: 25px;
		}
		
		h1, h2 {
			color: #1C60CF;
			margin: 0 auto;
			padding: 10px;
		}
		
		#payForm ul {
			list-style-image: url(../img/galochka.png);
		}

		.imp {
			background-color: pink;
			padding-bottom: 10px;
			border-radius: 5px;
		}
		#payForm {
			width: 510px;
			margin: 0 auto;
		}
		#frame {
			border-radius: 5px;
			border: 3px solid orange;
			margin: 0 auto;
			padding: 10px;
			padding-left: 75px;
			background-color: yellow;
		}
		#frame:hover {
			border-radius: 5px;
			border: 3px solid yellow;
			margin: 0 auto;
			padding: 10px;
			padding-left: 75px;
			background-color: orange;
		}
	</style>

 
</head>

<body>
    <header>
        <div class="overlay0">
                <div class="header1">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="logo">
                                    <a href="index.html"><img src="../img/hostellogo.png" width="60" height="60" alt="logo"><span class="lg1">Mama Hostel</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <h2>г. Одесса, ул. Нежинская, 58</h2>
                            </div>
                            <div class="col-sm-3">
                                <p class="tell">+38 068 792 72 10
                                    <br> +38 066 429 75 78
									<br><a href="mailto:mamahostel.od@gmail.com">mamahostel.od@gmail.com</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div>

                    <nav class="navbar navbar-inverse" role="navigation">
                        
						<div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav">
                                <li><a href="../#three">Номера и цены</a>
                                </li>
                                <li><a href="../#whywe">Почему Mama Hostel</a>
                                </li>
                                <li><a href="../#slider">Хостел в деталях</a>
                                </li>
                                <li><a href="../#otz">Отзывы</a>
                                </li>
                                <li><a href="../#howto">Как добраться</a>
                                </li>
                            </ul>
                        </div>
                        <!--.nav-collapse -->

                    </nav>


<div class='content'>

	<div id="payForm">
	<h1>Страница предоплаты</h1>

		<ul>
		    <li>Далее вы перейдете на сайт банка для ввода реквизитов.</li>
			<li>Совершая предоплату, вы соглашаетесь с <a href="../rules/">Правилами проживаня</a></li>
		    <li>Я принимаю во внимание, что платежная система моежт взымать до 3% комиссии за платеж.</li>
		            </ul>            
		<iframe width="508" height="588" style="border: none;" src="https://kaznachey.ua/PaymentForm/ru-RU/MwAxADIAOAA3ACYAMgBkADAANABjADQAZgA1ADQAMgBiADIAOQAwADIAYwA4ADgAYwA4ADAAZgA3ADQAYQAzAGMANQA5AGMANwAyAA=="></iframe>	
		<ul>
			<strong>
			<li><h4>Альтернативный способ оплаты: </h4><br><div id="frame">пополните карту Приват Банка <br># 4731 1856 0499 1510 Наумкин Александр</div></li>
			</strong>
		</ul>
	</div>
                    
	
		
		<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function (d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter29927109 = new Ya.Metrika({
						id:29927109,
						clickmap:true,
						trackLinks:true,
						accurateTrackBounce:true,
						webvisor:true,
						trackHash:true
					});
				} catch(e) { }
			});

			var n = d.getElementsByTagName("script")[0],
				s = d.createElement("script"),
				f = function () { n.parentNode.insertBefore(s, n); };
			s.type = "text/javascript";
			s.async = true;
			s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else { f(); }
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript><div><img src="//mc.yandex.ru/watch/29927109" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->
													

	
</body>

</html>



