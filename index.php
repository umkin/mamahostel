<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Хостел в центре Одессы">
    <meta name="author" content="http://mama-hostel.com">
    <meta name="verify-reformal" content="1032dfd62da4a9d76aec5004" />
    <title>Mama Hostel in Odessa</title>
    <link rel="shortcut icon" href="./favicon.ico">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="source/jquery.fancybox.css" type="text/css" media="screen">
    <link rel="stylesheet/less" href="css/style.less">
    <link rel="stylesheet" href="css/style.css">

    <!-- Put this script tag to the <head> of your page -->
    <script type="text/javascript" src="http://vk.com/js/api/share.js?93" charset="windows-1251"></script>

</head>

<body>
    <header>
        <div class="overlay0">
            <div class="header1">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="logo">
                                <a href="index.html"><img src="img/hostellogo.png" width="60" height="60" alt="logo"><span class="lg1">Mama Hostel</span>
                                </a>
                                
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <p class="od"> <address style="font-size: 2em"><a href="https://goo.gl/maps/z4Bkosnv6bv" target="_blank">г. Одесса, ул. Нежинская, 58</a></address>    </p>
                        </div>
                        <div class="col-sm-3">
                            <p class="tell"><a href="tel:+380687927210">+38 068 792 72 10</a>
                                <br><a href="tel:+380664297578">+38 066 429 75 78</a>
                                <br><a href="mailto:mamahostel.od@gmail.com">mamahostel.od@gmail.com</a>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <a href="http://vk.com/mama_hostel" target="_blanc">
                                    <img src="/img/vk.png" alt=""> <br> VK news
                                </a>
                        </div>
                        <div class="col-md-4"><a href="https://www.facebook.com/mamahostel/" target="_blanc">
                                    <img src="/img/facebook.png" alt=""> <br> FB news
                                </a></div>
                        <div class="col-md-4"><a href="https://telegram.me/odessahostels" target="_blanc">
                                    <img src="/img/telegram.png" alt=""> <br> Telegram
                                </a></div>
                    </div>
                </div>
            </div>
            
            <div>
                <nav class="navbar navbar-inverse" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <ul class="nav navbar-nav">
                        <li><a href="#three">Номера и цены</a>
                        </li>
                        <li><a href="#whywe">Почему Mama Hostel</a>
                        </li>
                        <li><a href="#slider">Хостел в деталях</a>
                        </li>
                        <li><a href="#otz">Отзывы</a>
                        </li>
                        <li><a href="#howto">Как добраться</a>
                        </li>
                        <li><a href="/rules/index.php">Правила</a>
                        </li>
                        <li><a href="/pay">Оплатить</a></li>
                    </ul>
                    <!--.nav-collapse -->
                </nav>
                <div class="container">
                    <div class="row">
                        <h1>
							<div id="zag">
                    			Хостел по домашнему в центре Одессы
							</div>
                			</h1>
                    </div>
                    <div class="row" id="bullets">
                        <div class="col-sm-6">
                            <br>
                            <br>
                            <p title="При проживании двоих стоимость 329">
                                Номер "Люкс" от 329
                            </p>
                            <p title="Стомиость недельного проживания 550">
                                Неделя проживания по 85 грн в день
                            </p>
<!--                            <p title="Предложение действует только с сентября по апрель.">-->
<!--                                Целый месяц всего по 55 грн в день*-->
<!--                            </p> -->
                            <p>Отдельная комната для девочек</p>
                            <!-- <p title="Скидки на тариф 'месяц' и 'неделя' не распространяются ">
                                Группам предоставляется скидки    
                            </p> -->
                            

                        </div>
                        <div class="col-sm-offset-1 col-sm-4">
                            <div class="form-bg text-center">
                                <div>
                                    <div class="form">
                                        <div class="form-area">
                                            <span class="bron">ЗАБРОНИРОВАТЬ СЕЙЧАС</span>
                                            <br>
                                            <form action="#">
                                                <div class="form-group">
                                                    <div class="col-sm-offset-1 col-sm-10">
                                                        <input type="text" placeholder="Имя" class="form-control">
                                                    </div>
                                                    <br>
                                                    <br>
                                                    <div class=" col-sm-offset-1 col-sm-10">
                                                        <input type="text" placeholder="email" class="form-control">
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <a href="http://www.booking.com/hotel/ua/mama-hostel.html?aid=330843;lang=ru " target="_blank" class="btn btn-warning center-block" target="_blank">
                                                ЗАБРОНИРОВАТЬ
                                            </a>
                                                <br>
                                                <br>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
        </div>
    </header>
    <section id="two" class="text-center hidden-xs">
        <div class="container">
            <div class="row">
                <br>
                <div class="centered text-center">
                    <span class="text-center span1">
                   Преимущества нашего хостела
               </span>
                </div>
                <div class="col-sm-4">
                    <p>
                        хостел в центре
                        <br> Одессы
                        <br>
                        <img src="img/crossroads-icon-96.jpg" width="96" height="96" alt="cross">
                    </p>
                </div>
                <div class="col-sm-4">
                    <p>
                        ресепшн
                        <br> 24 часа
                        <br>
                        <img src="img/clock-icon-96.jpg" width="96" height="96" alt="clock">
                    </p>
                </div>
                <div class="col-sm-4">
                    <p>
                        Евроремонт
                        <br> Ежедневная уборка
                        <br>
                        <img src="img/flower-icon-96.jpg" width="96" height="96" alt="flw">
                    </p>
                </div>
            </div>
            <br>
            <br>
            <br>
            <div class="row">
                <div class="col-sm-4">
                    <p>
                        Free wifi, tv,
                        <br>
                        <img src="img/wi-fi-icon-96.jpg" width="96" height="96" alt="cross">
                    </p>
                </div>
                <div class="col-sm-4">
                    <p>
                        мини кухня, лаундж зона
                        <br>
                        <img src="img/cup-icon-96.jpg" width="96" height="96" alt="clock">
                    </p>
                </div>
                <div class="col-sm-4">
                    <p>
                        3 сан.узла,
                        <br>
                        <img src="img/cmyk-icon-96.jpg" width="96" height="96" alt="flw">
                    </p>
                </div>
            </div>
        </div>
    </section>
    <br>
    <br>
    <br>
    <section id="three">
        <div class="container">
            <div class="row">
                <br>
                <div class="text-center">
                    <h3 class="visible-xs">
                                Номера и цены в нашем хостеле
                           </h3>
                    <span class="span1 hidden-xs">
                                Номера и цены в <span>нашем</span> хостеле
                    </span>
                </div>
            </div>
            <br>
            <br>
            <div class="row">
                <div class="col-sm-offset-2 col-sm-4">
                    <h4>
                        Люкс для двоих
                    </h4>
                    <ul>
                        <li>Полная автономия </li>
                        <li>Душ, туалет</li>
                        <li>Фен - кондиционер</li>
                        <li>Телевизор - не большой шкаф</li>
                        <li>Двухместная кровать</li>
                    </ul>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>Цена за 1 чел/номер<br>
                                    Двухспальная кровать</td>
                                <td>329</td>
                            </tr>
                            <tr>
                                <td>Цена за 2 чел/номер<br>
                                    Двухспальная кровать</td>
                                <td>369</td>
                            </tr>
                        </tbody>
                    </table>
                    <br class="visible-xs">
                    <br class="visible-xs">
                    <a id="fancybox-manual-c" href="javascript:;" class="btn btn-block btn-primary">Еще фото номера</a>
                    <a href="http://www.booking.com/hotel/ua/mama-hostel.html?aid=330843;lang=ru " target="_blank" class="btn btn-warning center-block">
                                                ЗАБРОНИРОВАТЬ
                                            </a>
                </div>
                <div class="col-sm-4">
                    <div class="thumbnail">
                        <a href="img/lux2.jpg" class="fancybox-effects-a"><img src="img/lux2.jpg" width="640" height="427" alt="lux1" class="img-responsive">
                        </a>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-offset-2 col-sm-4">
                    <h4>
                        4-х местный общий номер 
                    </h4>
                    <ul>
                        <li>У каждой кровати тумбочки </li>
                        <li>Светлая комната, большое окно</li>
                        <li>Кондиционер</li>
                        <li>можно для компаний</li>
                    </ul>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>Цена за человека</td>
                                <td>145</td>
                            </tr>
                        </tbody>
                    </table>
                    <br class="visible-xs">
                    <br class="visible-xs">
                    <a id="fancybox-manual-c1" href="javascript:;" class="btn btn-block btn-primary">Еще фото номера</a>
                    <a href="http://www.booking.com/hotel/ua/mama-hostel.html?aid=330843;lang=ru " target="_blank" class="btn btn-warning center-block">
                                                ЗАБРОНИРОВАТЬ
                                            </a>
                </div>
                <div class="col-sm-4">
                    <div class="thumbnail">
                        <a class="fancybox-effects-a" href="img/4room4.jpg"><img src="img/4room4.jpg" width="640" height="427" alt="4room" class="img-responsive">
                        </a>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-offset-2 col-sm-4">
                    <h4>
                        8-ми местная для девушек
                    </h4>
                    <ul>
                        <li>Номер с балконом в одесский дворик </li>
                        <li>Персональные тумбочки у каждой кровати</li>
                        <li>Кондиционер</li>
                    </ul>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>Количество суток</td>
                                <td>от 1</td>
                                <td>от 7ми</td>
<!--                                <td>от 20ти</td>-->
                            </tr>
                            <tr>
                                <td>Цена за человека в номере</td>
                                <td>129 грн</td>
                                <td>85 грн </td>
<!--                                <td>50 грн</td>-->
                            </tr>
                        </tbody>
                    </table>
                    <br class="visible-xs">
                    <br class="visible-xs">
                    <a id="fancybox-manual-c2" href="javascript:;" class="btn btn-primary btn-block">Еще фото номера</a>
                    <a href="http://www.booking.com/hotel/ua/mama-hostel.html?aid=330843;lang=ru " target="_blank" class="btn btn-warning center-block">
                                                ЗАБРОНИРОВАТЬ
                                            </a>
                </div>
                <div class="col-sm-4">
                    <div class="thumbnail">
                        <a href="img/8room2.jpg" class="fancybox-effects-a"><img src="img/8room2.jpg" width="640" height="427" alt="4room" class="img-responsive">
                        </a>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-offset-2 col-sm-4">
                    <h4>
                        8-ми местная общая комната 
                    </h4>
                    <ul>
                        <li>В комнате дополнительный мягкий уголок </li>
                        <li>Розетки у кровати</li>
                        <li>Кондиционер</li>
                        <li>Два окна</li>
                    </ul>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>Количество суток</td>
                                <td>от 1</td>
                                <td>от 7ми</td>
<!--                                <td>от 20ти</td>-->
                            </tr>
                            <tr>
                                <td>Цена за человека в номере</td>
                                <td>129 грн</td>
                                <td>85 грн </td>
<!--                                <td>50 грн</td>-->
                            </tr>
                        </tbody>
                    </table>
                    <br class="visible-xs">
                    <br class="visible-xs">
                    <a id="fancybox-manual-c3" href="javascript:;" class="btn btn-primary btn-block">Еще фото номера</a>
                    <a href="http://www.booking.com/hotel/ua/mama-hostel.html?aid=330843;lang=ru " target="_blank" class="btn btn-warning center-block">
                                                ЗАБРОНИРОВАТЬ
                                            </a>
                </div>
                <div class="col-sm-4">
                    <div class="thumbnail">
                        <a href="img/88room5.jpg" class="fancybox-effects-a"><img src="img/88room5.jpg" width="640" height="427" alt="4room" class="img-responsive">
                        </a>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-6">
                    <h4>Кухня и лаунж зона</h4>
                </div>
                <div class="col-sm-6">
                    <h4>Удобства</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <p class="thumbnail">
                        <a href="img/launge6.jpg" class="fancybox-effects-a">
                            <img src="img/launge6.jpg" width="640" height="427" alt="kithc" class="img-responsive">
                        </a>
                    </p>
                    <a id="fancybox-manual-c4" href="javascript:;" class="btn btn-block btn-primary">Еще фото</a>
                </div>
                <div class="col-sm-3">
                    <ul>
                        <li>Встроенная кухня с полным набором бытовой техники: плита, чайник, микроволновка, холодильник, ТВ. </li>
                        <li>Набор посуды</li>
                        <li>Большой отдельный стол для компаний.</li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <p class="thumbnail">
                        <a href="img/closet1%20(5).jpg" width="640" height="427" class="fancybox-effects-a">
                            <img src="img/closet1%20(5).jpg" alt="kithc" class="img-responsive">
                        </a>
                    </p>
                    <a id="fancybox-manual-c5" href="javascript:;" class="btn btn-block btn-primary">Еще фото</a>
                </div>
                <div class="col-sm-3">
                    <ul>
                        <li>Три сан узла в хостеле</li>
                        <li>Фен и утюг</li>
                        <li>Биде</li>
                        <li>Большие удобные зеркала</li>
                    </ul>
                    <br>
                    <br>
                    <br>
                </div>
            </div>
        </div>
    </section>
    <section id="whywe">
        <div class="container">
            <div class="row text-center">
                <br>
                <br>
                <h4 class="text-center">
                    Почему вам стоит остановиться именно в Мама Хостеле?
                </h4>
                <h3 class="text-center visible-xs">
                    Проживание в сердце Одессы
                </h3>
                <span class="span1 hidden-xs">
                     Проживание в сердце Одессы
                </span>
            </div>
            <br>
            <br>
            <div class="row">
                <div class=" col-sm-4">
                    <figure class="effect1 center-block"> </figure>
                    <figure class="effect6 center-block"> </figure>
                    <figure class="effect4 center-block"> </figure>
                    <figure class="effect5 center-block"> </figure>
                </div>
                <div class="col-sm-4 hidden-xs">
                    <img src="img/street_lamp_6aroow.jpg" width="326" height="525" alt="" class="img-responsive">
                </div>
                <div class=" col-sm-3">
                    <br>
                    <br>
                    <br>
                    <figure class="effect7 center-block"></figure>
                    <figure class="effect8 center-block"></figure>
                    <figure class="effect2 center-block"></figure>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-offset-3 col-sm-4">
                    <h4>
                        Ну и как же без моря в Одессе?
                    </h4>
                    <p>
                        Пляж Аркадия 25 минут езды
                    </p>
                </div>
                <div class="col-sm-3">
                    <p>
                        <img src="img/arkadia.JPG" width="520" height="347" alt="" class="img-responsive thumbnail">
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-offset-3 col-sm-4">
                    <p>
                        Пляж Ланжерон 15 мин езды
                    </p>
                </div>
                <div class="col-sm-3">
                    <p>
                        <img src="img/lanjeron.jpg" width="640" height="427" alt="" class="img-responsive thumbnail">
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section id="also">
        <div class="container">
            <div class="row visible-xs text-center">
                <br>
                <h2>
                   Преимущества нашего хостела
               </h2>
                <div class="col-sm-4">
                    <p>
                        хостел в центре
                        <br> Одессы
                        <br>
                        <img src="img/crossroads-icon-96.jpg" width="96" height="96" alt="cross">
                    </p>
                </div>
                <div class="col-sm-4">
                    <p>
                        ресепшн
                        <br> 24 часа
                        <br>
                        <img src="img/clock-icon-96.jpg" width="96" height="96" alt="clock">
                    </p>
                </div>
                <div class="col-sm-4">
                    <p>
                        Евроремонт
                        <br> Ежедневная уборка
                        <br>
                        <img src="img/flower-icon-96.jpg" width="96" height="96" alt="flw">
                    </p>
                </div>
            </div>
            <br>
            <br>
            <br>
            <div class="row visible-xs text-center">
                <div class="col-sm-4">
                    <p>
                        Free wifi, tv,
                        <br>
                        <img src="img/wi-fi-icon-96.jpg" width="96" height="96" alt="cross">
                    </p>
                </div>
                <div class="col-sm-4">
                    <p>
                        мини кухня, лаундж зона
                        <br>
                        <img src="img/cup-icon-96.jpg" width="96" height="96" alt="clock">
                    </p>
                </div>
                <div class="col-sm-4">
                    <p>
                        3 сан.узла,
                        <br>
                        <img src="img/cmyk-icon-96.jpg" width="96" height="96" alt="flw">
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="center-images">
                    <h3 class="text-center">А так же:</h3>
                    <div class="col-sm-4">
                        <p class="text-center">
                            <img src="img/creditcard-icon-96.jpg" width="96" height="96" alt="cruise">
                            <br> Возможна безналичная
                            <br> оплата картами Visa, MasterCard
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            <img src="img/cruise-icon-96.jpg" width="96" height="96" alt="cruise">
                            <br>
                            <br> Экскурсии по самым
                            <br> красивым местам Одессы
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-center">
                            <img src="img/briefcase-icon-96.jpg" width="96" height="96" alt="cruise">
                            <br> Рабочая зона для
                            <br> комфортной работы и отдыха
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="bron">
        <div class="overlay0">
            <div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <div class="priglash">
                                <h5>Приезжайте в Одессу! Наш хостел создан для молодых и абициозных людей, для путешественников и независимых личностей.</h5>
                                <h5>
                  Пожалуйста, введите даты визита в Одессу
               </h5>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class=" col-sm-12">
                            <div class="zakaz">
                                <script async type="text/javascript" src="http://www.booking.com/general.html?tmpl=bookit;aid=330843;lang=ru;hotel_id=1074362;cc1=ua;hotel_page=mama-hostel"></script>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                </div>
            </div>
        </div>
    </section>
    <section id="slider">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <br>
                    <h3 class="text-center visible-xs">
                        Мама Хостел в деталях
                    </h3>
                    <span class="span1 hidden-xs">Мама Хостел в деталях</span>
                    <br>
                    <br>
                    <div class="carousel1">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <img src="img/slide1.jpg" width="640" height="427" alt="slide">
                                </div>
                                <div class="item">
                                    <img src="img/slide2.jpg" width="640" height="427" alt="slide">
                                </div>
                                <div class="item">
                                    <img src="img/slide3.jpg" width="640" height="427" alt="slide">
                                </div>
                            </div>
                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <br>
                    <br>
                    <br>
                </div>
            </div>
        </div>
    </section>
    <section id="otz">
        <div class="overlayw">
            <div class="container">
                <div class="row text-center">
                    <br>
                    <br>
                    <br>
                    <h3 class="text-center visible-xs">Отзывы наших посетителей</h3>
                    <span class="span1 hidden-xs">Отзывы наших посетителей</span>
                </div>
                <div class="row text-center">
                    <div class="col-sm-6">
                        <div class="media">
                            <div class="media-left">
                                <a href="#">
                                    <img class="media-object" src="img/no-photo.png" width="150" height="150" alt="otz">
                                </a>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Анна Кузьо</h4>
                                <p><b>Проживала в общем восьмиместном номере</b>
                                </p>
                                <p>Все понравилось: очень доброжелательный персонал, чистота идеальная, месторасположение просто отличное: все рядом - магазины, Дерибасовская, хорошая транспортная развязка. Тишина и спокойная обстановка.”</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="media">
                            <div class="media-left">
                                <a href="#">
                                    <img class="media-object" src="img/ekater.png" width="150" height="150" alt="otz">
                                </a>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Катерина Сёмина </h4>
                                <p><b>Проживала в Люксе</b>
                                </p>
                                <p>Персонал приветливый, расположение хорошее,хорошая транспортная развязка,в номере тихо. Номер уютный</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="media">
                            <div class="media-left">
                                <a href="#">
                                    <img class="media-object" width="150" height="150" src="img/andrey.png" alt="otz">
                                </a>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Юрий Бондарчук </h4>
                                <p><b>Проживал в общем восьмиместном номере</b>
                                </p>
                                <p>В целом всё очень понравилось, особенно хостел подкупает своим домашним уютом, нехарактерным для такого типа заведений. Замечательный ремонт, всё очень чисто, всё необходимое для полноценной жизни есть. 5 минут пешком до Деребасовской</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="media">
                            <div class="media-left">
                                <a href="#">
                                    <img class="media-object" src="img/no-photo.png" width="150" height="150" alt="otz">
                                </a>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Анонимный</h4>
                                <p><b>Проживал в 4-х местной</b>
                                </p>
                                <p>Отличный хостел в центре города!Есть все необходимое для комфортного проживания.Открылись недавно,все новое.Скоростной WiFi.Приятный администратор - Марина!!Мне все очень порнавилось,было все уютно,по-домашнему!В следущий раз только в Мама хостел и в Вип комнату!!!)))”</p>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <br>
                <div class="text-center">
                    <h4>Остальные отзывы смотрите по ссылке</h4>
                    <a href="http://www.booking.com/hotel/ua/mama-hostel.ru.html?sid=f2a15525a7e51e4f37125cb1f0261717;dcid=1;ucfs=1;srfid=c6631480d07d2e62a398f76baa70bbda198c05ffX1#tab-reviews " target="_blank">Смотреть отзывы на booking.com</a>
                </div>
                <hr>
                <br>
                <br>
            </div>
        </div>
    </section>
    <section id="howto">
        <div class="container">
            <div class="row text-center">
                <h3 class="text-center visible-xs">
                    Как добраться до Мама Хостела?
                </h3>
                <span class="span1 hidden-xs">Как добраться до Мама Хостела?</span>
                <br>
                <br>
                <div class="col-sm-offset-2 col-sm-4">
                    <figure id="zd" class="center-block"></figure>
                    <p>
                        На привокзальной площади сесть на маршрутку 221 или 124, доехать до остановки ул. Толстого «обжора»., 100 метров до 58 дома, Код на двери 370, четвертый этаж, кв. 24.
                    </p>
                </div>
                <div class="col-sm-4">
                    <figure id="avto" class="center-block">
                    </figure>
                    <p>
                        Маршрутка 124 до остановки ул. Толстого «обжора»., 100 метров до 58 дома, Код на двери 370, четвертый этаж, кв. 24.
                    </p>
                </div>
            </div>
            <br>
            <br>
        </div>
    </section>
    <section id="map">
        <div class="row">
            <div class="col-sm-12">
                <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=FhvWe28XmSY1aI_xuyN7oV7YGcXFHz9N&width=100%&height=350"></script>
            </div>
        </div>
    </section>
    <section id="last">
        <div class="overlay0">
            <div>
                <div>
                    <div class="container">
                        <div class="row">
                            <br>
                            <h1 class="text-center">
                    Насладись уютом и комфортом в Мама Хостел
                </h1>
                        </div>
                        <div class="row" id="bullets1">
                            <div class="col-sm-6">
                                <br>
                                <br>
                                <p title="При проживании двоих стоимость 399">
                                Номер "Люкс" от 249 
                            </p>
                            <p title="Стомиость недельного проживания 550">
                                Неделя проживания по 78,<sup>42</sup> грн в день
                            </p>
                            <p title="Предложение действует только с сентября по апрель.">
                                Целый месяц всего по 50 грн в день*
                            </p>
                            <p title="Скидки на тариф 'месяц' не распространяются ">
                                Группам предоставляется скидки    
                            </p>
                            </div>
                            <div class="col-sm-offset-1 col-sm-4">
                                <div class="form-bg text-center">
                                    <span class="bron">ЗАБРОНИРОВАТЬ СЕЙЧАС</span>
                                    <br>
                                    <span>прямо сейчас по выгодной цене</span>
                                    <div>
                                        <form action="#">
                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-8">
                                                    <input type="text" placeholder="Имя" class="form-control">
                                                </div>
                                                <br>
                                                <br>
                                                <div class=" col-sm-offset-2 col-sm-8">
                                                    <input type="text" placeholder="email" class="form-control">
                                                </div>
                                            </div>
                                            <br>
                                            <br>
                                            <a href="http://www.booking.com/hotel/ua/mama-hostel.html?aid=330843;lang=ru " class="btn btn-warning center-block" target="_blank">
                                                ЗАБРОНИРОВАТЬ
                                            </a>
                                            <br>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header1">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="logo">
                                    <a href="index.html"><img src="img/hostellogo.png" width="60" height="60" alt="logo"><span class="lg1">Mama Hostel</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <p class="od">г.Одесса, ул. Нежинская 58,
                                    <br>код 370, кв. 24</p>
                            </div>
                            <div class="col-sm-3">
                                <p class="tell">+38-066-429-75-78
                                    <br> +38-068-792-72-10
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <br>
                <br>
            </div>
        </div>
    </section>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function () {
                try {
                    w.yaCounter29927109 = new Ya.Metrika({
                        id: 29927109,
                        clickmap: true,
                        trackLinks: true,
                        accurateTrackBounce: true,
                        webvisor: true,
                        trackHash: true
                    });
                } catch (e) {}
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () {
                    n.parentNode.insertBefore(s, n);
                };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript>
        <div><img src="//mc.yandex.ru/watch/29927109" style="position:absolute; left:-9999px;" alt="" />
        </div>
    </noscript>
    <!-- /Yandex.Metrika counter -->

    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <!--[if IE]>
        <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript" src="source/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="source/helpers/jquery.fancybox-buttons.js"></script>
    <script type="text/javascript" src="source/helpers/jquery.fancybox-media.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/less.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            /*
             *  Simple image gallery. Uses default settings
             */

            $('.fancybox').fancybox();

            /*
             *  Different effects
             */

            // Change title type, overlay closing speed
            $(".fancybox-effects-a").fancybox({
                helpers: {
                    title: {
                        type: 'outside'
                    },
                    overlay: {
                        speedOut: 0
                    }
                }
            });

            // Disable opening and closing animations, change title type
            $(".fancybox-effects-b").fancybox({
                openEffect: 'none',
                closeEffect: 'none',

                helpers: {
                    title: {
                        type: 'over'
                    }
                }
            });

            // Set custom style, close if clicked, change title type and overlay color
            $(".fancybox-effects-c").fancybox({
                wrapCSS: 'fancybox-custom',
                closeClick: true,

                openEffect: 'none',

                helpers: {
                    title: {
                        type: 'inside'
                    },
                    overlay: {
                        css: {
                            'background': 'rgba(238,238,238,0.85)'
                        }
                    }
                }
            });

            // Remove padding, set opening and closing animations, close if clicked and disable overlay
            $(".fancybox-effects-d").fancybox({
                padding: 0,

                openEffect: 'elastic',
                openSpeed: 150,

                closeEffect: 'elastic',
                closeSpeed: 150,

                closeClick: true,

                helpers: {
                    overlay: null
                }
            });

            /*
             *  Button helper. Disable animations, hide close button, change title type and content
             */

            $('.fancybox-buttons').fancybox({
                openEffect: 'none',
                closeEffect: 'none',

                prevEffect: 'none',
                nextEffect: 'none',

                closeBtn: false,

                helpers: {
                    title: {
                        type: 'inside'
                    },
                    buttons: {}
                },

                afterLoad: function () {
                    this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
                }
            });


            /*
             *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
             */

            $('.fancybox-thumbs').fancybox({
                prevEffect: 'none',
                nextEffect: 'none',

                closeBtn: false,
                arrows: false,
                nextClick: true,

                helpers: {
                    thumbs: {
                        width: 50,
                        height: 50
                    }
                }
            });

            /*
             *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
             */
            $('.fancybox-media')
                .attr('rel', 'media-gallery')
                .fancybox({
                    openEffect: 'none',
                    closeEffect: 'none',
                    prevEffect: 'none',
                    nextEffect: 'none',

                    arrows: false,
                    helpers: {
                        media: {},
                        buttons: {}
                    }
                });

            /*
             *  Open manually
             */

            $("#fancybox-manual-a").click(function () {
                $.fancybox.open('1_b.jpg');
            });

            $("#fancybox-manual-b").click(function () {
                $.fancybox.open({
                    href: 'iframe.html',
                    type: 'iframe',
                    padding: 5
                });
            });

            $("#fancybox-manual-c").click(function () {
                $.fancybox.open([
                    {
                        href: 'img/lux1.jpg',
                        title: '2-х местный люкс'
     }, {
                        href: 'img/lux2.jpg',
                        title: '2-х местный люкс'
     }, {
                        href: 'img/lux3.jpg',
                        title: '2-х местный люкс'
     }
                    , {
                        href: 'img/lux4.jpg',
                        title: '2-х местный люкс'
     }
    ], {
                    helpers: {
                        thumbs: {
                            width: 75,
                            height: 50
                        }
                    }
                });
            });
            $("#fancybox-manual-c1").click(function () {
                $.fancybox.open([
                    {
                        href: 'img/4room.jpg',
                        title: '4-х местный '
     }, {
                        href: 'img/4room2.jpg',
                        title: '4-х местный '
     }, {
                        href: 'img/4room3.jpg',
                        title: '4-х местный '
     },
                    {
                        href: 'img/4room4.jpg',
                        title: '4-х местный '
     },
                    {
                        href: 'img/4room5.jpg',
                        title: '4-х местный '
     }

    ], {
                    helpers: {
                        thumbs: {
                            width: 75,
                            height: 50
                        }
                    }
                });
            });
            $("#fancybox-manual-c2").click(function () {
                $.fancybox.open([
                    {
                        href: 'img/8room.jpg',
                        title: '8-х местный '
     }, {
                        href: 'img/8room1.jpg',
                        title: '8-х местный '
     }, {
                        href: 'img/8room2.jpg',
                        title: '8-х местный '
     },
                    {
                        href: 'img/8room3.jpg',
                        title: '8-х местный '
     }

    ], {
                    helpers: {
                        thumbs: {
                            width: 75,
                            height: 50
                        }
                    }
                });
            });
            $("#fancybox-manual-c3").click(function () {
                $.fancybox.open([
                    {
                        href: 'img/88room.jpg',
                        title: '8-х местный '
     }, {
                        href: 'img/88room1.jpg',
                        title: '8-х местный '
     }, {
                        href: 'img/88room3.jpg',
                        title: '8-х местный '
     },
                    {
                        href: 'img/88room4.jpg',
                        title: '8-х местный '
     }
                    ,
                    {
                        href: 'img/88room5.jpg',
                        title: '8-х местный '
     }
                    ,
                    {
                        href: 'img/88room6.jpg',
                        title: '8-х местный '
     }
                    ,
                    {
                        href: 'img/88room7.jpg',
                        title: '8-х местный '
     }

    ], {
                    helpers: {
                        thumbs: {
                            width: 75,
                            height: 50
                        }
                    }
                });
            });
            $("#fancybox-manual-c4").click(function () {
                $.fancybox.open([
                    {
                        href: 'img/launge.jpg',
     }, {
                        href: 'img/launge1.jpg',
     }, {
                        href: 'img/launge2.jpg',
     },
                    {
                        href: 'img/launge3.jpg',
     }
                    ,
                    {
                        href: 'img/launge7.jpg',
     }
                    ,
                    {
                        href: 'img/launge5.jpg',
     }
                    ,
                    {
                        href: 'img/launge6.jpg',
     },
                    {
                        href: 'img/launge6.jpg',
     },
                    {
                        href: 'img/launge8.jpg',
     },
                    {
                        href: 'img/launge9.jpg',
     },
                    {
                        href: 'img/launge10.jpg',
     },
                    {
                        href: 'img/launge11.jpg',
     },
                    {
                        href: 'img/launge12.jpg',
     },
                    {
                        href: 'img/launge13.jpg',
     }

    ], {
                    helpers: {
                        thumbs: {
                            width: 75,
                            height: 50
                        }
                    }
                });
            });



        });
    </script>

    <script type="text/javascript">
    var reformalOptions = {
        project_id: 952438,
        project_host: "mamahostel.reformal.ru",
        force_new_window: true,
        tab_orientation: "right",
        tab_indent: "50%",
        tab_bg_color: "#F05A00",
        tab_border_color: "#FFFFFF",
        tab_image_url: "http://tab.reformal.ru/T9GC0LfRi9Cy0Ysg0Lgg0L%252FRgNC10LTQu9C%252B0LbQtdC90LjRjw==/FFFFFF/88128dfd6ca0743b5ccc2f8afed9f3b1/right/0/tab.png",
        tab_border_width: 1
    };
    
    (function() {
        var script = document.createElement('script');
        script.type = 'text/javascript'; script.async = true;
        script.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'media.reformal.ru/widgets/v3/reformal.js';
        document.getElementsByTagName('head')[0].appendChild(script);
    })();
</script><noscript><a href="http://reformal.ru"><img src="http://media.reformal.ru/reformal.png" /></a><a href="http://mamahostel.reformal.ru">Oтзывы и предложения для Mama Hostel in Odessa</a></noscript>

</body>

</html>