<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Хостел в центре Одессы">
    <meta name="author" content="http://mama-hostel.com">
	<meta http-equiv="Refresh" content="3;url=http://mama-hostel.com/">
    <title>Mama Hostel in Odessa</title>
    
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../source/jquery.fancybox.css" type="text/css" media="screen" >
    <link rel="stylesheet/less" href="../css/style.less">

 
</head>

<body>
    <header>
        <div class="overlay0">
                <div class="header1">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="logo">
                                    <a href="index.html"><img src="../img/hostellogo.png" alt="logo"><span class="lg1">Mama Hostel</span>
                                    </a>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div>

                  
                    <div class="container">
                        <div class="row">
                            <h1>
                    Этой страницы больше не существует
                </h1>
                        </div>
                        <div class="row" id="bullets">
                            <div class="col-sm-6">
                                <br>
                                <br>
                                <h2>
                                   Через три секунды вы попадете в нужное место
                                </h2>
                                
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </header>
    
	
</body>

</html>