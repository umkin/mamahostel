<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Хостел в центре Одессы">
    <meta name="author" content="http://mama-hostel.com">
    <title>Правила - Mama Hostel in Odessa</title>
    
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../source/jquery.fancybox.css" type="text/css" media="screen" >
    <link rel="stylesheet/less" href="../css/style.less">
	
	<style>
	
		body {
			background-color:  #CCD9F9;
		}
		
		.content {
			width: 75%;
			margin: 0 auto;
			background-color:  #ddD9F9;
			padding: 25px;
		}
		
		h1, h2 {
			color: #1C60CF;
			margin: 0 auto;
			padding: 10px;
		}
		
		ul {
			list-style-image: url(../img/galochka.png);
		}

		.imp {
			background-color: pink;
			padding-bottom: 10px;
			border-radius: 5px;
		}
	</style>

 
</head>

<body>
    <header>
        <div class="overlay0">
                <div class="header1">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="logo">
                                    <a href="index.html"><img src="../img/hostellogo.png" width="60" height="60" alt="logo"><span class="lg1">Mama Hostel</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <h2>г. Одесса, ул. Нежинская, 58</h2>
                            </div>
                            <div class="col-sm-3">
                                <p class="tell">+38 068 792 72 10
                                    <br> +38 066 429 75 78
									<br><a href="mailto:mamahostel.od@gmail.com">mamahostel.od@gmail.com</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div>

                    <nav class="navbar navbar-inverse" role="navigation">
                        
						<div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav">
                                <li><a href="../#three">Номера и цены</a>
                                </li>
                                <li><a href="../#whywe">Почему Mama Hostel</a>
                                </li>
                                <li><a href="../#slider">Хостел в деталях</a>
                                </li>
                                <li><a href="../#otz">Отзывы</a>
                                </li>
                                <li><a href="../#howto">Как добраться</a>
                                </li>
                            </ul>
                        </div>
                        <!--.nav-collapse -->

                    </nav>


<div class='content'>
	<h1>Правила проживания в Мама Хостеле !</h1>
	
	<strong><h5 class="imp">За нарушение правил, выделенных таким цветом, администрация имеет право выселить нарушителей в любое время без возвращения платы за проживание за текущие и следующие сутки</h5></strong>

	<ol>
		
		
		<h2>
			<li>Расчетное время.</li>
		</h2>	
			<ul>
				<li>Поселение в хостел происходит с 12:00. В последний день проживания до 11 утра необходимо освободить кровать, или договориться с администратором хостела о другом.</li>
			</ul>
		<h2>
			<li>Оплата и регистрация.</li>
		</h2>
			<ul>
				<li>При регистрации необходимо предъявить документ для подтверждения личных данных (паспорт, водительские права) менеджеру хостела и осуществить оплату сразу за ВСЕ дни проживания.</li>
				<li>Минимальный срок проживания -  сутки.</li>
				<li>Если вы вдруг переночуете в другом месте, не уведомив администрацию заблаговременно - денег уже не вернешь. </li>
				
				<li>При бронировании проживания следует оплатить залог в размере стоимости первых суток за каждого жильца. При поселении групп более 4 человек правило может обсуждаться дополнительно.</li>
				<li>Перенести или бронирование можно не позднее чем за 72 часа до заезда, причем только при помощи электронной почты.</li>
				<li>Все актуальные цены и тарифы на проживание обозначена на нашем сайте <a href="http://mama-hostel.com">mama-hostel.com</a> в национальной валюте за 1 сутки проживания соответственно категории койко-места.</li>
				<li>Расчет за проживание возможен наличными или переводом на карту Приват Банка</li>
			</ul>
		
		<h2>
			<li>На территории Хостела запрещено!!!</li>
		</h2>
			<ul>
				<li class="imp">Курить, употреблять крепкие алкогольные напитки, наркотические и психотропные вещества.</li>
				<li class="imp">Вести себя дерзко, шуметь, громко разговаривать (особенно в ночное время), кричать, нецензурно выражаться, оскорблять других.</li>
				<li>Оставлять свои личные вещи и полотенца в местах общего пользования (кухня, душ, туалет), сумки и рюкзаки в коридоре, а также ценные вещи и документы в номере на время твоего отсутствия.</li>
				<li>Приводить гостей в ночное время (после 22:00).</li>
				<li>Передавать посторонним лицам ключи от комнаты.</li>
				<li class="imp">Хранить оружие, наркотические и психотропные вещества.</li>
				<li>Переставлять мебель в номере без разрешения дежурного администратора.</li>
				<li>Держать животных и птиц.</li>
				<li class="imp">Нарушать покой других гостей, которые тоже проживают в Хостеле.</li>
			</ul>
		
		<h2>
			<li>Гость обязан:</li>
		</h2>
			<ul>
				<li>Придерживаться установленных в Хостеле правил проживания.</li>
				<li>Придерживаться чистоты. В  Хостеле Мама самообслуживание, поэтому мой за собой посуду, убирай со стола. Тоже самое касается душевых кабин и туалета. Относись с уважением к тем, кто зайдёт после тебя.</li>
				<li>Принимай душ ежедневно, используй чистое белье и носки. </li>
				<li>Возместить ущерб, в случае потери или порчи имущества Хостела соответственно действующего законодательства.</li>
				<li>Выходя из Хостела, необходимо оставить ключ от комнаты и входной двери в дежурного администратора на рецепции. За потерю ключей штраф – 100 грн.</li>
			</ul>
			
		<h2>
			<li>Мы оставляем за собой право отказать в проживании или немедленно выселить гостя, который:</li>
		</h2>
			<ul>
				<li>Не согласен с правилами нашего заведения.</li>
				<li class="imp">Мешает комфортному пребыванию в Хостеле другим гостям.</li>
				<li class="imp">Пребывает в нетрезвом состоянии или неадекватно себя ведет.</li>
				<li>Младше 18 лет без соответственного разрешения родителей или сопровождения взрослых.</li>
				<li>Не имеет при себе документа, что подтверждает его личность.</li>
			</ul>
		
		<h2>
			<li>У нас не нужно платить за:</li>
		</h2>
			<ul>
				<li>Wi-Fi</li>
				<li>Пользование утюгом и всем оборудованием Хостела. (Кроме стиральной машины)</li>
				<li>Пользование постельным бельем, в т.ч. полотенцем, тапочками и берушами.</li>
				<li>Вызов такси.</li>
				<li>Парковку велосипеда.</li>
				
			</ul>
		
		<h2>
			<li>В Хостеле можно получить дополнительные платные услуги:</li>
		</h2>
			<ul>
				<li>Пользование стиральной машиной – 20 грн.</li>
			</ul>
		
		
		<h2>
			<li>Общие правила:</li>
		</h2>
			<ul>
				<li>Уборка в общих зонах проводится каждый день с 8:00 до 10:00.</li>
				<li>Уборка в комнатах проводится каждый день с 10:00 до 12:00.</li>
				<li>Замена постельного белья каждые семь дней на одного заказчика.</li>
				<li>а порчу имущества Хостела взимается штраф в размере, установленном администрацией Хостела.</li>
				<li></li>
			</ul>
	</ol>
</div>
                    
	
		
		<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function (d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter29927109 = new Ya.Metrika({
						id:29927109,
						clickmap:true,
						trackLinks:true,
						accurateTrackBounce:true,
						webvisor:true,
						trackHash:true
					});
				} catch(e) { }
			});

			var n = d.getElementsByTagName("script")[0],
				s = d.createElement("script"),
				f = function () { n.parentNode.insertBefore(s, n); };
			s.type = "text/javascript";
			s.async = true;
			s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else { f(); }
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript><div><img src="//mc.yandex.ru/watch/29927109" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->
													

	
</body>

</html>



